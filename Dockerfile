FROM docker.twun.io/node:15.14.0-alpine3.13

COPY . /pak

WORKDIR /pak

RUN set -eu;\
  adduser -h /pak pak -g '' -s /bin/sh -S -D -H -u 4050;\
  chown -fR pak /pak;\
  chmod -fR u+rwX /pak

USER pak

RUN yarn install --frozen-lockfile

ENTRYPOINT ["bin/pak"]
