module.exports = {
  env: {
    es6: true,
    node: true
  },
  extends: [
    'eslint:recommended',
    'plugin:ante/recommended',
    'plugin:ante/style'
  ],
  parser: '@babel/eslint-parser',
  parserOptions: {
    ecmaFeatures: {
      experimentalObjectRestSpread: true
    },
    ecmaVersion: 6,
    sourceType: 'module'
  },
  plugins: [
    'eslint-plugin-ante'
  ],
  rules: {
    'id-blacklist': [
      'error',
      'addr',
      'arr',
      'cb',
      'db',
      'e',
      'err',
      'evt',
      'img',
      'req',
      'res'
    ],
    'require-atomic-updates': ['off']
  }
};
