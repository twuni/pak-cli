require('@babel/register')({
  plugins: [
    'babel-plugin-istanbul'
  ],
  presets: [
    [
      '@babel/preset-env',
      {
        targets: {
          node: '15'
        }
      }
    ]
  ]
});
